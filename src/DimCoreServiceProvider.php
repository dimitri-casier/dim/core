<?php

namespace Dim\Core;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class DimCoreServiceProvider extends ServiceProvider
{

    public function register()
    {

    }

    public function boot()
    {

    }
}
