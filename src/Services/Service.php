<?php

namespace Dim\Core\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\Validator;

abstract class Service
{
    protected $model;
    protected $rules = [];
    protected $errors = [];
    protected $validationMessages = [];
    protected $validationAttributes = [];

    public function __construct(Model $model)
    {
        $this->model = $model;

    }

    protected function validate($data, $rules)
    {
        $validator = Validator::make($data, $rules, $this->validationMessages, $this->validationAttributes);
        if ($validator->fails())
            $this->setErrors($validator->getMessageBag()->toArray());

    }

    protected function getLocale()
    {
        return App::getLocale();
    }

    public function hasErrors()
    {

        return count($this->errors) > 0;
    }

    public function setErrors($source)
    {

        if ($source instanceof Validator) {
            $this->errors = $source->getMessageBag()->toArray();
            return;
        }//endif

        $this->errors = $source;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function present($data)
    {

        return $data;
    }

    protected function presentTranslationInRecord($record)
    {


        if (!isset($record["translations"]))
            return $record;

        $translations = [];
        foreach ($record["translations"] as $translation)
            $translations[$translation['language']] = $this->presentTranslationColumnsInRecord($translation);

        $record["translations"] = $translations;

        return $record;
    }

    protected function presentTranslationColumnsInRecord($translation)
    {
        return $translation;
    }

    protected function addPagingAndFetch($query, $queryString = [], $pages = 20)
    {
        $result = $query->paginate($pages)
            ->withPath('')
            ->appends($queryString);

        return $result;
    }

    protected function collectFieldValues($results, $field)
    {
        $values =  collect($results)->map(function($record) use($field){
            return $record->$field;
        });

        return $values->unique();
    }

    protected function parseToDictionary($results, $fieldAsKey, $dictionary = []){
        foreach ($results as $record) {
            $key = $record[$fieldAsKey];
            $dictionary[$key] = $record;
        }

        return $dictionary;
    }

    protected function getRules($key){
        return isset($this->rules[$key]) ? $this->rules[$key] : "";
    }


}
